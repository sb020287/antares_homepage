function imageSliderBasic() {
    const slider = document.querySelector('.image-canvas .image-slider');
    const images = Array.from(slider.getElementsByTagName('img'));
    let currentIndex = 0;
  
    function showImage(index) {
      images.forEach((image, i) => {
        if (i === index) {
          image.classList.add('active');
        } else {
          image.classList.remove('active');
        }
      });
    }
  
    function nextImage() {
      currentIndex = (currentIndex + 1) % images.length;
      showImage(currentIndex);
    }
  
    setInterval(nextImage, 3000); // Change image every 3 seconds
  }
  
  imageSliderBasic();
  
  
  