const css = `
#publications {
  list-style-type: none;
  padding: 0;
}

#publications li {
  display: flex;
  align-items: left;
  padding: 5px;
  margin-bottom: 5px;
  background-color: #E3E1D4;
  border: 1px solid #4c3224;
}

#publications .numbering {
  flex-shrink: 0;
  width: 30px;
  text-align: left;
  font-weight: bold;
}

#publications .content {
  flex-grow: 1;
}

#publications .section {
  margin-bottom: 5px;
}

#publications .subtitle {
  font-weight: bold;
}
`;


const style = document.createElement('style');
style.textContent = css;
document.head.appendChild(style);

// List of DOIs
const dois = [
  "10.3847/1538-4357/aca8b0",
  "10.3847/2041-8213/acaeae",
  "10.3847/1538-4357/abd3a6",
  "10.3847/1538-4357/ac70cc",
  "10.1016/j.parco.2022.102904",
  "10.3847/1538-4365/abca97",
  "10.1088/0067-0049/221/1/4",
  "10.1088/0004-637X/800/2/106",
  "10.1088/0004-637X/775/2/118",
  "10.1093/mnras/stv662",
  "10.1088/0004-637X/814/1/72",
  "10.3847/0004-637X/830/2/111",
  "10.1088/0004-637X/744/2/136",
  "10.3847/0067-0049/224/2/16",
  "10.1088/0004-637X/805/1/32",
  "10.1088/2041-8205/759/1/L22",
  "10.1088/0004-637X/739/2/102",
  "10.1088/0004-637X/731/1/15",
  "10.1016/j.jcp.2012.08.003",
  "10.1088/0004-637X/759/1/59",
  "10.3847/1538-4365/ac9279",
  "10.1088/0004-637X/785/2/103",
  "10.1088/0004-637X/764/2/123",
  "10.1088/0004-637X/790/1/32",
  "10.1046/j.1365-8711.2003.06608.x",
  "10.1088/0004-637X/771/1/8",
  "10.3847/1538-4365/ab397b",
  "10.1088/0004-637X/746/1/74",
  "10.1088/0004-637X/785/2/152",
  "10.1086/590247",
  "10.1086/528707",
  "10.1086/506513",
  "10.1088/0004-637X/773/1/49",
  "10.3847/1538-4357/abceca",
  "10.1023/B:ASTR.0000045038.70412.a3",
  "10.1016/j.newast.2011.06.013",
  "10.5303/JKAS.2005.38.2.197",
  "10.1086/380602",
  "10.1086/379243",
  "10.1086/379242",
  "10.1023/A:1021321017685",
  "10.1080/03091929.2010.508455",
  "10.1051/0004-6361:20011713",
  "10.1088/0004-637X/760/1/21",
  "10.1088/0004-637X/806/1/27",
  "10.1016/j.newast.2014.01.001",
  "10.1088/0004-637X/715/1/78",
  "10.1088/0004-637X/721/2/1878",
  "10.1093/mnras/sty1166",
  "10.1088/0004-637X/792/1/71",
  "10.1086/345544",
  "10.1051/0004-6361:20021892",
  "10.1088/1674-4527/13/1/008",
  "10.1007/s00159-013-0059-2",
  "10.1088/0004-637X/774/1/70",
  "10.1093/mnras/sty947",
  "10.3847/1538-4357/ab0307",
  "10.1103/PhysRevD.98.023523",
  "10.3847/1538-4357/aac5e8",
  "10.3847/1538-4357/ac4d2e",
  "10.3847/1538-4357/ac7de9"
];


// Function to fetch citation info
async function fetchCitationInfo(doi) {
  const res = await fetch(
    `https://api.crossref.org/works/${doi}/transform/application/vnd.citationstyles.csl+json`
  );
  const data = await res.json();
  return data;
}

async function fetchPublication(doi) {
    const res = await fetch(`https://api.crossref.org/works/${doi}`);
    const data = await res.json();
  
    // Only define values if they exist in the data
    const authors = data.message.author
      ? data.message.author.map((author) => {
          const authorName = `${author.given} ${author.family}`;
          const isBold = [
            "Hsien Shang",
            "Ruben Krasnopolsky",
            "Somdeb Bandopadhyay",
            "Miljenko Čemeljić",
            "Miikka S. Väisälä",
            "Min-Kai Lin",
            "Yao-HuanTseng",
            "Kouichi Hirotani",
            "Pin-Gao Gu",
            "Ronald E. Taam",
            "Chun-Fan Liu",
            "David C. C. Yen",
            "Chien-Chang Yen",
          ].includes(authorName); // Add the specific author names here
          return isBold ? `<b>${authorName}</b>` : authorName;
        })
      : null;
  
    let formattedAuthors = "";
    if (authors && authors.length > 0) {
      if (authors.length === 1) {
        formattedAuthors = authors[0];
      } else if (authors.length === 2) {
        formattedAuthors = authors.join(" & ");
      } else {
        const lastAuthor = authors.pop();
        formattedAuthors = `${authors.join(", ")} & ${lastAuthor}`;
      }
    }
  
    const title = data.message.title ? data.message.title[0] : null;
    const doiLink = `https://doi.org/${doi}`;
    const adsLink = `https://ui.adsabs.harvard.edu/abs/${doi}`;
    const fullTextLink = data.message["link"]
      ? data.message["link"][0]["URL"]
      : null;
    const journal = data.message["container-title"]
      ? data.message["container-title"][0]
      : null;
  
    // Extract the year and month from the publication date
    const dateParts = data.message["issued"]["date-parts"]
      ? data.message["issued"]["date-parts"][0]
      : null;
    const year = dateParts ? dateParts[0] : null;
    const month = dateParts ? dateParts[1] : null;
    const publicationDate = dateParts ? new Date(year, month - 1) : null;
    
    const dateShow = data.message["issued"]["date-parts"]
    ? data.message["issued"]["date-parts"][0].length > 1
    ? `${data.message["issued"]["date-parts"][0][1]}/${data.message["issued"]["date-parts"][0][0]}` // month/year
    : data.message["issued"]["date-parts"][0][0] // year only
    : null;
  
    return {
      authors: formattedAuthors,
      title,
      doiLink,
      adsLink,
      fullTextLink,
      journal,
      publicationDate,
      dateShow,
    };
  }
  
  
// Function to create publication list
async function createPublicationList() {
    const ol = document.getElementById("publications");


      // Add loading comment
      const loadingComment = document.createElement("li");
      loadingComment.textContent = "Loading publications...";
      ol.appendChild(loadingComment);
  
    // Fetch publication data
    const promises = dois.map(fetchPublication);
    const results = await Promise.allSettled(promises);
  
    // Filter and extract successful results
    const publications = results
      .filter(result => result.status === "fulfilled")
      .map(result => result.value);
  

     // Remove loading comment
    ol.removeChild(loadingComment);

    // Sort the publications based on publication date (most recent first)
    publications.sort((a, b) => b.publicationDate - a.publicationDate);
  
    for (let i = 0; i < publications.length; i++) {
      const publication = publications[i];
  
      // Create list item
      const li = document.createElement("li");
  
      // Only add values to HTML if they are not null
      li.innerHTML = `
        <div class="numbering">${i + 1})</div>&nbsp&nbsp
        <div class="content">
          ${publication.authors ? `<div class="section"><span class="subtitle">Author(s): </span>${publication.authors}</div>` : ""}
          ${publication.title ? `<div class="section"><span class="subtitle">Title: </span>${publication.title}</div>` : ""}
          ${publication.journal ? `<div class="section"><span class="subtitle">Journal: </span>${publication.journal}</div>` : ""}
          ${publication.dateShow ? `<div class="section"><span class="subtitle">Date: </span>${publication.dateShow}</div>` : ""}
          ${publication.doiLink ? `<div class="section"><span class="subtitle">DOI: </span><a href="${publication.doiLink}">${publication.doiLink}</a></div>` : ""}
          <div class="section">
            <span class="subtitle">Links: </span>
            ${publication.adsLink ? `<a href="${publication.adsLink}">[ADS]</a>` : ""}
            ${publication.fullTextLink ? `<a href="${publication.fullTextLink}">[FULLTEXT]</a>` : ""}
          </div>
        </div>
      `;
  
      // Append list item to list
      ol.appendChild(li);
    }
  }

// Create publication list on page load
createPublicationList();
