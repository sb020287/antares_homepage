function updateButtonWidth() {
  const canvas = document.querySelector('.image-canvas');
  const canvasWidth = canvas.offsetWidth;
  const controls = canvas.querySelector('.controls');
  const playPauseBtn = controls.querySelector('.play-pause');
  const buttonCount = controls.children.length;
  const totalButtonWidth = playPauseBtn.offsetWidth;
  const availableWidth = canvasWidth - totalButtonWidth + buttonCount * 5; // Considering 5px margin for each button
  const buttonWidth = Math.floor(availableWidth / buttonCount);

  controls.style.setProperty('--button-width', buttonWidth + 'px');
}

function imageSlider() {
  const canvas = document.querySelector('.image-canvas');
  const slider = canvas.querySelector('.image-slider');
  const images = Array.from(slider.getElementsByTagName('img'));
  const controls = canvas.querySelector('.controls');
  const prevBtn = controls.querySelector('.prev');
  const playPauseBtn = controls.querySelector('.play-pause');
  const nextBtn = controls.querySelector('.next');
  let currentIndex = 0;
  let isPlaying = true;
  let intervalId;

  images.forEach((image) => {
    image.style.position = 'absolute';
    image.style.width = '100%';
    image.style.height = '100%';
    image.style.objectFit = 'cover';
    image.style.transition = 'opacity 1s ease-in-out';
  });

  function showImage(index) {
    images.forEach((image, i) => {
      if (i === index) {
        image.style.opacity = '1';
      } else {
        image.style.opacity = '0';
      }
    });
  }

  function nextImage() {
    currentIndex = (currentIndex + 1) % images.length;
    showImage(currentIndex);
  }

  function prevImage() {
    currentIndex = (currentIndex - 1 + images.length) % images.length;
    showImage(currentIndex);
  }

  function playSlider() {
    playPauseBtn.textContent = 'Pause';
    intervalId = setInterval(nextImage, 3000);
  }

  function pauseSlider() {
    playPauseBtn.textContent = 'Play';
    clearInterval(intervalId);
  }

  function togglePlayPause() {
    if (isPlaying) {
      pauseSlider();
    } else {
      playSlider();
    }
    isPlaying = !isPlaying;
  }

  prevBtn.addEventListener('click', prevImage);
  nextBtn.addEventListener('click', nextImage);
  playPauseBtn.addEventListener('click', togglePlayPause);
  window.addEventListener('resize', updateButtonWidth);

  showImage(currentIndex);
  updateButtonWidth();
  playSlider();
}

imageSlider();
